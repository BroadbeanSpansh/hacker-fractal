use strict;
use warnings;

use Bean::Fractal;
use Test::More;

my $fractal = Bean::Fractal->new( { width => 1000 , height => 1000 } );
my ( $x, $y ) = $fractal->calculate_start_point;
is $x, 500;
is $y, 1000;

is (int($fractal->calculate_line_length(1)),333);


done_testing;
