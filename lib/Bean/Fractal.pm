package Bean::Fractal;
use strict;

use Moose;
use GD::Simple;

use constant SCALE => 1/2;
use constant ANGLE => 120;
use constant BRANCHES => 8;

has steps => ( isa => 'Int', is => 'rw' );

has width => ( isa => 'Int', is => 'rw' );
has height => ( isa => 'Int', is => 'rw' );

has gd => ( is => 'ro', builder => '_build_gd', lazy => 1 );

sub _build_gd {
    my $self =shift;
    return GD::Simple->new( $self->width, $self->height );
}

sub create_tree {
    my ($self,$filename) = @_;

    my ($x,$y) = $self->calculate_start_point;

    $self->_recurse_tree(
        x => $x,
        y => $y,
        angle => -90,
    );

    open my $file,">$filename" || die "Unable to open $!";
    binmode $file;
    print $file $self->gd->png;
    close $file;
}

sub _recurse_tree {
    my ($self) = shift;
    my %args = @_;

    my $depth = $args{depth} || 1;

    if ($depth > $self->steps) {
        return;
    }
    my $length = $self->calculate_line_length($depth);
    if ( $length < 1) {
        return;
    }

    my $gd = $self->gd;
    $gd->moveTo($args{x},$args{y});
    $gd->penSize(1,1);
    $gd->bgcolor('black');
    $gd->fgcolor('black');
    $gd->angle($args{angle});
    $gd->line($length);
    my ($x,$y) = $gd->curPos;

    my $branch_angle = ANGLE/(BRANCHES-1);
    for (my $i=0; $i < BRANCHES; $i++ ) {
        $self->_recurse_tree(
            x => $x,
            y => $y,
            angle => $args{angle} - ANGLE/2 + ($i * $branch_angle),
            depth => $depth + 1,
        );
    }

}

sub calculate_start_point {
    my $self = shift;
    return ( $self->width/2, $self->height);
}

sub calculate_line_length {
    my ($self,$depth) = @_;

    return (SCALE ** $depth) * $self->height;
}

1;

