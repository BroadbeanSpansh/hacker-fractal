#!/usr/bin/env perl

use strict;
use lib qw( lib );

use Bean::Fractal;

my $fractal = Bean::Fractal->new(
    width => 10000,
    height => 10000,
    steps => 10,
);

$fractal->create_tree('output3.png');
